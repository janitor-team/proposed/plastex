Source: plastex
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Stuart Prescott <stuart@debian.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 12),
 dh-python,
 python3-all,
 python3-pytest,
Build-Depends-Indep:
 dvipng,
 libjs-jquery,
 pdf2svg,
 python3-bs4,
 python3-chardet,
 python3-docopt,
 python3-jinja2,
 python3-pil,
 python3-unidecode,
 texlive-binaries,
 texlive-latex-base,
 texlive-latex-extra,
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/plastex
Vcs-Git: https://salsa.debian.org/python-team/packages/plastex.git
Homepage: https://github.com/plastex/plastex
Rules-Requires-Root: no

Package: plastex
Architecture: all
Depends:
 python3,
 python3-plastex,
 ${misc:Depends}
Breaks:
 python-plastex
Replaces:
 python-plastex
Section: tex
Description: LaTeX document processing framework in Python
 plasTeX is a collection of Python frameworks that allow you to process LaTeX
 documents. This processing includes, but is not limited to, conversion of
 LaTeX documents to various document formats. Of course, it is capable of
 converting to HTML or XML formats such as DocBook and tBook, but it is an
 open framework that allows you to drive any type of rendering. This means that
 it could be used to drive a COM object that creates a MS Word Document.
 .
 The plasTeX framework allows you to control all of the processes including
 tokenizing, object creation, and rendering through API calls. You also have
 access to all of the internals such as counters, the states of "if" commands,
 locally and globally defined macros, labels and references, etc. In essence,
 it is a LaTeX document processor that gives you the advantages of an XML
 document in the context of a language as superb as Python.
 .
 This package contains the command line tool 'plastex'.

Package: python3-plastex
Architecture: all
Depends:
 dvipng,
 python3-chardet,
 python3-pil,
 python3-unidecode,
 texlive-latex-base,
 ${misc:Depends},
 ${python3:Depends}
Recommends:
 libjs-jquery,
 python3-jinja2,
Suggests:
 pdf2svg,
 python3-cheetah,
 python3-genshi,
 texlive-latex-extra,
Replaces:
 python-plastex
Description: LaTeX document processing framework in Python - modules
 plasTeX is a collection of Python frameworks that allow you to process LaTeX
 documents. This processing includes, but is not limited to, conversion of
 LaTeX documents to various document formats. Of course, it is capable of
 converting to HTML or XML formats such as DocBook and tBook, but it is an
 open framework that allows you to drive any type of rendering. This means that
 it could be used to drive a COM object that creates a MS Word Document.
 .
 The plasTeX framework allows you to control all of the processes including
 tokenizing, object creation, and rendering through API calls. You also have
 access to all of the internals such as counters, the states of "if" commands,
 locally and globally defined macros, labels and references, etc. In essence,
 it is a LaTeX document processor that gives you the advantages of an XML
 document in the context of a language as superb as Python.
 .
 This package contains the Python 3 modules.

Package: python-plastex-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends}
Suggests:
 doc-base
Description: LaTeX document processing framework in Python - documentation
 plasTeX is a collection of Python frameworks that allow you to process LaTeX
 documents. This processing includes, but is not limited to, conversion of
 LaTeX documents to various document formats. Of course, it is capable of
 converting to HTML or XML formats such as DocBook and tBook, but it is an
 open framework that allows you to drive any type of rendering. This means that
 it could be used to drive a COM object that creates a MS Word Document.
 .
 The plasTeX framework allows you to control all of the processes including
 tokenizing, object creation, and rendering through API calls. You also have
 access to all of the internals such as counters, the states of "if" commands,
 locally and globally defined macros, labels and references, etc. In essence,
 it is a LaTeX document processor that gives you the advantages of an XML
 document in the context of a language as superb as Python.
 .
 This package contains the documentation for both modules and command line
 tool.
